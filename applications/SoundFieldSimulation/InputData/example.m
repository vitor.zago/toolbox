clear all; clc; close all;
materialA = itaPorousModel();
materialA.modelName = 'Wood';
materialA.fDensity = 8.4;
materialA.fFluidBulkModulus = 1;
materialA.sDensity = 10;
materialA.sBulkModuls = 20;
materialA.sVolumeFraction = 30;

materialA.IsModelComplete()
