classdef itaPorousModel
    %itaPorousModel represents a porous material model (also called equivalent fluid model).
    %
    % The equivalent fluid is considered to present a complex, frequency-dependent characteristic density 
    % and a dynamic bulk modulus of the combined equivalent fluid-solid system (saturating fluid and porous matrix).
    % There are empirical, physical and semi-phenomenological equivalent fluid models.
    %
    % Properties:
    %   modelName
    % 
    % See also ...
    %
    %   Reference page in Help browser
    %       <a href="matlab:doc itaPorousModel">doc itaPorousModel</a>
    %
    % <ITA-Toolbox>
    % This file is part of the ITA-Toolbox. Some rights reserved.
    % You can find the license for this m-file in the license.txt file in the ITA-Toolbox folder.
    % </ITA-Toolbox>

    properties (SetAccess = private, Hidden = true)
        modelList = {'Attenbourough',...
                     'Wilson',...
                     'ZwikkerKosten',...
                     'JohnsonChampouxAllard',...
                     'JohnsonChampouxAllardLafarge',...
                     'JohnsonChampouxAllardPrideLafarge',...
                     'WilliamsEDFM',...
                     'Wood',...
                     'DelanyBazleyMiki'};
        preSettingList = {'DelanyBazley',...
                          'Miki',...
                          'Qunli',...
                          'MechelGlassFiberLowX',...
                          'MechelGlassFiberHighX',...
                          'Komatsu',...
                          'AllardChampoux'};
    end
    
    properties (SetObservable = true)

        %% Model name and presetting for Delany-Bazley-Miki
        modelName =                                                     [];
        preSetting =                                                    [];
                
        %% Fluid parameters
        fDensity =                                                      [],...
        fRatioOfSpecificHeats =                                         [],...
        fDynamicViscosity =                                             [],...
        fHeatCapacityAtConstantPressure =                               [],...
        fThermalConductivity =                                          [],...
        fSpeedOfSound =                                                 [],...
        fFluidBulkModulus =                                             [];
                    
        %% Solid parameters
        sPorosity =                                                     [];                                        
        sBulkModulusInfiniteFrequencyLimit =                            [];
        sDensityInfiniteFrequencyLimit =                                [];                    
        sEntropyModeRelaxationTime =                                    [];                              
        sVorticityModeRelaxationTime =                                  [];                    
        sFlowResistivity =                                                [];                                  
        sTortuosityFactor =                                             [];                                
        sHydraulicRadiusOfPores =                                       [];
        sBFittingParameter =                                            [];                           
        sViscousCharacteristicLength =                                  [];                     
        sThermalCharacteristicLength =                                  [];                     
        sStaticThermalPermeability =                                    [];                      
        sStaticViscousTortuosity =                                      [];                          
        sGrainDensity =                                                 [];                                    
        sGrainBulkModulus =                                             [];                                
        sPermeability =                                                 [];                                    
        sVolumeFraction =                                               [];                                  
        sBulkModuls =                                                   [];                                      
        sDensity =                                                      [];                                         
        sC1 =                                                           [];                                             
        sC2 =                                                           [];                                            
        sC3 =                                                           [];                                             
        sC4 =                                                           [];                                            
        sC5 =                                                           [];                                            
        sC6 =                                                           [];                                            
        sC7 =                                                           [];                                            
        sC8 =                                                           [];
        sC9 =                                                           [];
       
%         fDensity =                                                      Fluid.Density,...
%         fRatioOfSpecificHeats =                                         Fluid.RatioOfSpecificHeats,...
%         fDynamicViscosity =                                             Fluid.DynamicViscosity,...
%         fHeatCapacityAtConstantPressure =                               Fluid.HeatCapacityAtConstantPressure,...
%         fThermalConductivity =                                          Fluid.ThermalConductivity,...
%         fSpeedOfSound =                                                 Fluid.SpeedOfSound,...
%         fFluidBulkModulus =                                             Fluid.FluidBulkModulus;        
        
%         sPorosity =                                                     Solid.Porosity;                                        
%         sBulkModulusInfiniteFrequencyLimit =                            Solid.BulkModulusInfiniteFrequencyLimit;
%         sDensityInfiniteFrequencyLimit =                                Solid.DensityInfiniteFrequencyLimit;                    
%         sEntropyModeRelaxationTime =                                    Solid.EntropyModeRelaxationTime;                              
%         sVorticityModeRelaxationTime =                                  Solid.VorticityModeRelaxationTime;                    
%         sFlowResistivity =                                                Solid.FlowResistivity;                                  
%         sTortuosityFactor =                                             Solid.TortuosityFactor;                                
%         sHydraulicRadiusOfPores =                                       Solid.HydraulicRadiusOfPores;
%         sBFittingParameter =                                            Solid.BFittingParameter;                           
%         sViscousCharacteristicLength =                                  Solid.ViscousCharacteristicLength;                     
%         sThermalCharacteristicLength =                                  Solid.ThermalCharacteristicLength;                     
%         sStaticThermalPermeability =                                    Solid.StaticThermalPermeability;                      
%         sStaticViscousTortuosity =                                      Solid.StaticViscousTortuosity;                          
%         sGrainDensity =                                                 Solid.GrainDensity;                                    
%         sGrainBulkModulus =                                             Solid.GrainBulkModulus;                                
%         sPermeability =                                                 Solid.Permeability;                                    
%         sVolumeFraction =                                               Solid.VolumeFraction;                                  
%         sBulkModuls =                                                   Solid.BulkModuls;                                      
%         sDensity =                                                      Solid.Density;                                         
%         sC1 =                                                           Solid.C1;                                             
%         sC2 =                                                           Solid.C2;                                            
%         sC3 =                                                           Solid.C3;                                             
%         sC4 =                                                           Solid.C4;                                            
%         sC5 =                                                           Solid.C5;                                            
%         sC6 =                                                           Solid.C6;                                            
%         sC7 =                                                           Solid.C7;                                            
%         sC8 =                                                           Solid.C8;                                            
%         sC9 =                                                           Solid.C9;
    end
    
   methods
      function this = set.modelName(this, modelName)
            %assert(isstring(modelName), 'You can only assign an model to this object.')
            cop = sum(ismember(this.modelList,modelName));
            if cop
                this.modelName = modelName;
            else
                assert(logical(cop), 'This porousacoustic model is not available.')
            end
            return;
      end
      function this = set.preSetting(this, preSetting)
            %assert(isstring(modelName), 'You can only assign an model to this object.')
            cop = sum(ismember(this.preSettingList,preSetting));
            if cop
                this.preSettingList = preSetting;
            else
                assert(logical(cop), 'This porousacoustic model is not available.')
            end
            return;
      end
   end
      
   %% Fluid setters properties
   methods
      function this = set.fDensity(this,density)
          if isnumeric(density) && isempty(density)
                this.fDensity = [];
                return;
          end
          this.fDensity = density;
          return;
      end
      function this = set.fRatioOfSpecificHeats(this,ratioOfSpecificHeats)
          if isnumeric(ratioOfSpecificHeats) && isempty(ratioOfSpecificHeats)
                this.fRatioOfSpecificHeats = [];
                return;
          end
          this.fRatioOfSpecificHeats = ratioOfSpecificHeats;
          return;
      end
      function this = set.fDynamicViscosity(this,dynamicViscosity)
          if isnumeric(dynamicViscosity) && isempty(dynamicViscosity)
                this.fDynamicViscosity = [];
                return;
          end
          this.fDynamicViscosity = dynamicViscosity;
          return;
      end
      function this = set.fHeatCapacityAtConstantPressure(this,heatCapacityAtConstantPressure)
          if isnumeric(heatCapacityAtConstantPressure) && isempty(heatCapacityAtConstantPressure)
                this.fHeatCapacityAtConstantPressure = [];
                return;
          end
          this.fHeatCapacityAtConstantPressure = heatCapacityAtConstantPressure;
          return;
      end      
      function this = set.fThermalConductivity(this,thermalConductivity)
          if isnumeric(thermalConductivity) && isempty(thermalConductivity)
                this.fThermalConductivity = [];
                return;
          end
          this.fThermalConductivity = thermalConductivity;
          return;
      end
      function this = set.fSpeedOfSound(this,speedOfSound)
          if isnumeric(speedOfSound) && isempty(speedOfSound)
                this.fSpeedOfSound = [];
                return;
          end
          this.fSpeedOfSound = speedOfSound;
          return;
      end  
      function this = set.fFluidBulkModulus(this,fluidBulkModulus)
          if isnumeric(fluidBulkModulus) && isempty(fluidBulkModulus)
                this.fFluidBulkModulus = [];
                return;
          end
          this.fFluidBulkModulus = fluidBulkModulus;
          return;
      end
    end
   
   %% Solid setters properties
   methods
      function this = set.sPorosity(this,porosity)
          if isnumeric(porosity) && isempty(porosity)
              this.sPorosity = [];
              return;
          elseif ( porosity >= 0 ) && ( porsity <= 1 )
            disp('Porosity must have a value between 0 and 1')
              this.sPorosity = [];
              return;
          end
          this.sPorosity = porosity;
          return;
      end
      function this = set.sBulkModulusInfiniteFrequencyLimit(this,bulkModulusInfiniteFrequencyLimit)
          if isnumeric(bulkModulusInfiniteFrequencyLimit) && isempty(bulkModulusInfiniteFrequencyLimit)
                this.sBulkModulusInfiniteFrequencyLimit = [];
                return;
          end
          this.sBulkModulusInfiniteFrequencyLimit = bulkModulusInfiniteFrequencyLimit;
          return;
      end
      function this = set.sDensityInfiniteFrequencyLimit(this,densityInfiniteFrequencyLimit)
          if isnumeric(densityInfiniteFrequencyLimit) && isempty(densityInfiniteFrequencyLimit)
                this.sDensityInfiniteFrequencyLimit = [];
                return;
          end
          this.sDensityInfiniteFrequencyLimit = densityInfiniteFrequencyLimit;
          return;
      end     
      function this = set.sEntropyModeRelaxationTime(this,entropyModeRelaxationTime)
          if isnumeric(entropyModeRelaxationTime) && isempty(entropyModeRelaxationTime)
                this.sEntropyModeRelaxationTime = [];
                return;
          end
          this.sEntropyModeRelaxationTime = entropyModeRelaxationTime;
          return;
      end
      function this = set.sVorticityModeRelaxationTime(this,vorticityModeRelaxationTime)
          if isnumeric(vorticityModeRelaxationTime) && isempty(vorticityModeRelaxationTime)
                this.sVorticityModeRelaxationTime = [];
                return;
          end
          this.sVorticityModeRelaxationTime = vorticityModeRelaxationTime;
          return;
      end
      function this = set.sFlowResistivity(this,flowResistivity)
          if isnumeric(flowResistivity) && isempty(flowResistivity)
                this.sFlowResistivity = [];
                return;
          end
          this.sFlowResistivity = flowResistivity;
          return;
      end      
      function this = set.sTortuosityFactor(this,tortuosityFactor)
          if isnumeric(tortuosityFactor) && isempty(tortuosityFactor)
                this.sTortuosityFactor = [];
                return;
          end
          this.sTortuosityFactor = tortuosityFactor;
          return;
       end      
      function this = set.sHydraulicRadiusOfPores(this,hydraulicRadiusOfPores)
          if isnumeric(hydraulicRadiusOfPores) && isempty(hydraulicRadiusOfPores)
                this.sHydraulicRadiusOfPores = [];
                return;
          end
          this.sHydraulicRadiusOfPores = hydraulicRadiusOfPores;
          return;
       end      
      function this = set.sBFittingParameter(this,bFittingParameter)
          if isnumeric(bFittingParameter) && isempty(bFittingParameter)
                this.sBFittingParameter = [];
                return;
          end
          this.sBFittingParameter = bFittingParameter;
          return;
       end
      function this = set.sViscousCharacteristicLength(this,viscousCharacteristicLength)
          if isnumeric(viscousCharacteristicLength) && isempty(viscousCharacteristicLength)
                this.sViscousCharacteristicLength = [];
                return;
          end
          this.sViscousCharacteristicLength = viscousCharacteristicLength;
          return;
       end
      function this = set.sThermalCharacteristicLength(this,thermalCharacteristicLength)
          if isnumeric(thermalCharacteristicLength) && isempty(thermalCharacteristicLength)
                this.sThermalCharacteristicLength = [];
                return;
          end
          this.sThermalCharacteristicLength = thermalCharacteristicLength;
          return;
       end
      function this = set.sStaticThermalPermeability(this,staticThermalPermeability)
          if isnumeric(staticThermalPermeability) && isempty(staticThermalPermeability)
                this.sStaticThermalPermeability = [];
                return;
          end
          this.sStaticThermalPermeability = staticThermalPermeability;
          return;
       end
      function this = set.sStaticViscousTortuosity(this,staticViscousTortuosity)
          if isnumeric(staticViscousTortuosity) && isempty(staticViscousTortuosity)
                this.sStaticViscousTortuosity = [];
                return;
          end
          this.sStaticViscousTortuosity = staticViscousTortuosity;
          return;
       end
      function this = set.sGrainDensity(this,grainDensity)
          if isnumeric(grainDensity) && isempty(grainDensity)
                this.sGrainDensity = [];
                return;
          end
          this.sGrainDensity = grainDensity;
          return;
      end
      function this = set.sGrainBulkModulus(this,grainBulkModulus)
          if isnumeric(grainBulkModulus) && isempty(grainBulkModulus)
                this.sGrainBulkModulus = [];
                return;
          end
          this.sGrainBulkModulus = grainBulkModulus;
          return;
      end     
      function this = set.sPermeability(this,permeability)
          if isnumeric(permeability) && isempty(permeability)
                this.sPermeability = [];
                return;
          end
          this.sPermeability = permeability;
          return;
      end
      function this = set.sVolumeFraction(this,volumeFraction)
          if isnumeric(volumeFraction) && isempty(volumeFraction)
                this.sVolumeFraction = [];
                return;
          end
          this.sVolumeFraction = volumeFraction;
          return;
      end
      function this = set.sBulkModuls(this,bulkModuls)
          if isnumeric(bulkModuls) && isempty(bulkModuls)
                this.sBulkModuls = [];
                return;
          end
          this.sBulkModuls = bulkModuls;
          return;
      end
      function this = set.sDensity(this,density)
          if isnumeric(density) && isempty(density)
                this.sDensity = [];
                return;
          end
          this.sDensity = density;
          return;
      end
      function this = set.sC1(this,C1)
          if isnumeric(C1) && isempty(C1)
                this.sC1 = [];
                return;
          end
          this.sC1 = C1;
          return;
      end
      function this = set.sC2(this,C2)
          if isnumeric(C2) && isempty(C2)
                this.sC2 = [];
                return;
          end
          this.sC2 = C2;
          return;
      end
      function this = set.sC3(this,C3)
          if isnumeric(C3) && isempty(C3)
                this.sC3 = [];
                return;
          end
          this.sC3 = C3;
          return;
      end
      function this = set.sC4(this,C4)
          if isnumeric(C4) && isempty(C4)
                this.sC4 = [];
                return;
          end
          this.sC4 = C4;
          return;
      end
      function this = set.sC5(this,C5)
          if isnumeric(C5) && isempty(C5)
                this.sC5 = [];
                return;
          end
          this.sC5 = C5;
          return;
      end
      function this = set.sC6(this,C6)
          if isnumeric(C6) && isempty(C6)
                this.sC6 = [];
                return;
          end
          this.sC6 = C6;
          return;
      end
      function this = set.sC7(this,C7)
          if isnumeric(C7) && isempty(C7)
                this.sC7 = [];
                return;
          end
          this.sC7 = C7;
          return;
      end
      function this = set.sC8(this,C8)
          if isnumeric(C8) && isempty(C8)
                this.sC8 = [];
                return;
          end
          this.sC8 = C8;
          return;
      end
      function this = set.sC9(this,C9)
          if isnumeric(C9) && isempty(C9)
                this.sC9 = [];
                return;
          end
          this.sC9 = C9;
          return;
      end

   end
   
   %% Model testers
   methods
      function bool = HasModel(this)
         bool = ~isempty(this.modelName);
      end
      
      function bool = HasFdensity(this)
         bool = arrayfun(@(x) ~isempty(x.fDensity), this);
      end 
      function bool = HasFratioOfSpecificHeats(this)
         bool = arrayfun(@(x) ~isempty(x.fRatioOfSpecificHeats), this);
      end
      function bool = HasFdynamicViscosity(this)
         bool = arrayfun(@(x) ~isempty(x.fDynamicViscosity), this);
      end
      function bool = HasFheatCapacityAtConstantPressure(this)
         bool = arrayfun(@(x) ~isempty(x.fHeatCapacityAtConstantPressure), this);
      end
      function bool = HasFthermalConductivity(this)
         bool = arrayfun(@(x) ~isempty(x.fThermalConductivity), this);
      end
      function bool = HasFspeedOfSound(this)
         bool = arrayfun(@(x) ~isempty(x.fSpeedOfSound), this);
      end
      function bool = HasFfluidBulkModulus(this)
         bool = arrayfun(@(x) ~isempty(x.fFluidBulkModulus), this);
      end
     
      function bool = HasSporosity(this)
         bool = arrayfun(@(x) ~isempty(x.sPorosity), this);
      end
      function bool = HasSbulkModulusInfiniteFrequencyLimit(this)
         bool = arrayfun(@(x) ~isempty(x.sBulkModulusInfiniteFrequencyLimit), this);
      end
      function bool = HasSdensityInfiniteFrequencyLimit(this)
         bool = arrayfun(@(x) ~isempty(x.sDensityInfiniteFrequencyLimit), this);
      end 
      function bool = HasSentropyModeRelaxationTime(this)
         bool = arrayfun(@(x) ~isempty(x.sEntropyModeRelaxationTime), this);
      end
      function bool = HasSvorticityModeRelaxationTime(this)
         bool = arrayfun(@(x) ~isempty(x.sVorticityModeRelaxationTime), this);
      end 
      function bool = HasSflowResistivity(this)
         bool = arrayfun(@(x) ~isempty(x.sFlowResistivity), this);
      end
      function bool = HasStortuosityFactor(this)
         bool = arrayfun(@(x) ~isempty(x.sTortuosityFactor), this);
      end
      function bool = HasShydraulicRadiusOfPores(this)
         bool = arrayfun(@(x) ~isempty(x.sHydraulicRadiusOfPores), this);
      end
      function bool = HasSbFittingParameter(this)
         bool = arrayfun(@(x) ~isempty(x.sBFittingParameter), this);
      end 
      function bool = HasSviscousCharacteristicLength(this)
         bool = arrayfun(@(x) ~isempty(x.sViscousCharacteristicLength), this);
      end
      function bool = HasSthermalCharacteristicLength(this)
         bool = arrayfun(@(x) ~isempty(x.sThermalCharacteristicLength), this);
      end 
      function bool = HasSstaticThermalPermeability(this)
         bool = arrayfun(@(x) ~isempty(x.sStaticThermalPermeability), this);
      end
      function bool = HasSstaticViscousTortuosity(this)
         bool = arrayfun(@(x) ~isempty(x.sStaticViscousTortuosity), this);
      end
      function bool = HasSgrainDensity(this)
         bool = arrayfun(@(x) ~isempty(x.sGrainDensity), this);
      end
      function bool = HasSgrainBulkModulus(this)
         bool = arrayfun(@(x) ~isempty(x.sGrainBulkModulus), this);
      end
      function bool = HasSpermeability(this)
         bool = arrayfun(@(x) ~isempty(x.sPermeability), this);
      end
      function bool = HasSvolumeFraction(this)
         bool = arrayfun(@(x) ~isempty(x.sVolumeFraction), this);
      end
      function bool = HasSbulkModuls(this)
         bool = arrayfun(@(x) ~isempty(x.sBulkModuls), this);
      end
      function bool = HasSdensity(this)
         bool = arrayfun(@(x) ~isempty(x.sDensity), this);
       end     
      function bool = HasSC1(this)
         bool = arrayfun(@(x) ~isempty(x.sC1), this);
      end
      function bool = HasSC2(this)
         bool = arrayfun(@(x) ~isempty(x.sC2), this);
      end
      function bool = HasSC3(this)
         bool = arrayfun(@(x) ~isempty(x.sC3), this);
      end  
      function bool = HasSC4(this)
         bool = arrayfun(@(x) ~isempty(x.sC4), this);
      end  
      function bool = HasSC5(this)
         bool = arrayfun(@(x) ~isempty(x.sC5), this);
      end  
      function bool = HasSC6(this)
         bool = arrayfun(@(x) ~isempty(x.sC6), this);
      end 
      function bool = HasSC7(this)
         bool = arrayfun(@(x) ~isempty(x.sC7), this);
      end 
      function bool = HasSC8(this)
         bool = arrayfun(@(x) ~isempty(x.sC8), this);
      end 
      function bool = HasSC9(this)
         bool = arrayfun(@(x) ~isempty(x.sC9), this);
      end 
     
      function bool = HasPreSetting(this)
         bool = arrayfun(@(x) isstring(x.preSetting), this);
      end 
 
      function bool = IsModelComplete(this)
          assert( HasModel(this), 'The porousacoustic model has not been choosen!' );
          switch this.modelName
              case 'Attenbourough'
                  disp('Model: Attenbourough!');
                  assert(this.HasFdensity() && ...
                         this.HasFratioOfSpecificHeats() && ...
                         this.HasFdynamicViscosity() && ...
                         this.HasFheatCapacityAtConstantPressure() && ...
                         this.HasFthermalConductivity() && ...
                         this.HasSporosity() && ...
                         this.HasSflowResistivity() && ...
                         this.HasStortuosityFactor() && ...
                         this.HasSbFittingParameter(), ...
                         'There are parameters missing'); 
                  bool = 'True';
              case 'Wilson'
                  disp('Model: Wilson!');
                  assert(this.HasFdensity() && ...
                         this.HasFratioOfSpecificHeats() && ...
                         this.HasSporosity() && ...
                         this.HasSbulkModulusInfiniteFrequencyLimit() && ...
                         this.HasSdensityInfiniteFrequencyLimit() && ...
                         this.HasSentropyModeRelaxationTime() && ...
                         this.HasSvorticityModeRelaxationTime(), ...
                         'There are parameters missing'); 
                  bool = 'True';
              case 'ZwikkerKosten'
                  disp('Model: ZwikkerKosten!');
                  assert(this.HasFdensity() && ...
                         this.HasFheatCapacityAtConstantPressure() && ...
                         this.HasFratioOfSpecificHeats() && ...
                         this.HasFthermalConductivity() && ...
                         this.HasFdynamicViscosity() && ...
                         this.HasFspeedOfSound() && ...
                         this.HasSporosity() && ...
                         this.HasShydraulicRadiusOfPores(), ...
                         'There are parameters missing'); 
                  bool = 'True';
              case 'JohnsonChampouxAllard'
                  disp('Model: JohnsonChampouxAllard!');
                  assert(this.HasFdensity() && ...
                         this.HasFspeedOfSound() && ...
                         this.HasFheatCapacityAtConstantPressure() && ...
                         this.HasFdynamicViscosity() && ...
                         this.HasFratioOfSpecificHeats() && ...
                         this.HasFthermalConductivity() && ...
                         this.HasSporosity() && ...
                         this.HasSflowResistivity() && ...
                         this.HasSviscousCharacteristicLength() && ...
                         this.HasSthermalCharacteristicLength() && ...
                         this.HasStortuosityFactor(), ...
                         'There are parameters missing.'); 
                  bool = 'True';
              case 'JohnsonChampouxAllardLafarge'
                  disp('Model: JohnsonChampouxAllardLafarge!');
                  assert(this.HasFdensity() && ...
                         this.HasFspeedOfSound() && ...
                         this.HasSflowResistivity() && ...
                         this.HasSviscousCharacteristicLength() && ...
                         this.HasSthermalCharacteristicLength() && ...
                         this.HasStortuosityFactor() && ...
                         this.HasSstaticThermalPermeability() && ...
                         this.HasSstaticViscousTortuosity(),...
                         'There are parameters missing.'); 
                  bool = 'True';
              case 'JohnsonChampouxAllardPrideLafarge'
                   disp('Model: JohnsonChampouxAllardPrideLafarge!');
                   assert(this.HasFdensity() && ...
                          this.HasFspeedOfSound() && ...
                          this.HasFheatCapacityAtConstantPressure() && ...
                          this.HasFdynamicViscosity() && ...
                          this.HasFratioOfSpecificHeats() && ...
                          this.HasFthermalConductivity() && ...
                          this.HasSporosity() && ...
                          this.HasSflowResistivity() && ...
                          this.HasSviscousCharacteristicLength() && ...
                          this.HasSthermalCharacteristicLength() && ...
                          this.HasStortuosityFactor() && ...
                          this.HasSstaticThermalPermeability() && ...
                          this.HasSstaticViscousTortuosity() && ...
                          this.HasSgrainDensity(),...
                          'There are parameters missing.'); 
                  bool = 'True';
              case 'WilliamsEDFM'
                   disp('Model: WilliamsEDFM!');
                   assert(this.HasFdensity() && ...
                          this.HasFfluidBulkModulus() && ...
                          this.HasFdynamicViscosity() && ...
                          this.HasSgrainBulkModulus() && ...
                          this.HasSpermeability() && ...
                          this.HasSporosity() && ...
                          this.HasStortuosityFactor() && ...
                          this.HasSvolumeFraction() && ...
                          this.HasShydraulicRadiusOfPores(), ...
                         'There are parameters missing.'); 
                  bool = 'True';
              case 'DelanyBazleyMiki'
                   disp('Model: Delany-Bazley-Miki!');
                   assert(this.HasFdensity() && ...
                           this.HasFspeedOfSound() && ...
                           this.HasSflowResistivity() && ...
                           this.HasSC1() && ...
                           this.HasSC2() && ...
                           this.HasSC3() && ...
                           this.HasSC4() && ...
                           this.HasSC5() && ...
                           this.HasSC6() && ...
                           this.HasSC7() && ...
                           this.HasSC8() && ...
                           this.HasSC9(), ...
                           'There are parameters missing.');

%                    assert((this.HasFdensity() && ...
%                           this.HasFspeedOfSound() && ...
%                           this.HasSflowResistivity() && ...
%                           this.HasSC1() && ...
%                           this.HasSC2() && ...
%                           this.HasSC3() && ...
%                           this.HasSC4() && ...
%                           this.HasSC5() && ...
%                           this.HasSC6() && ...
%                           this.HasSC7() && ...
%                           this.HasSC8() && ...
%                           this.HasSC9()) || ...
%                           (this.HasPreSetting() ), ...
                  bool = 'True';
              case 'Wood'
                  disp('Model: Wood!');
                  assert(this.HasFdensity() & ...
                         this.HasFfluidBulkModulus() & ...
                         this.HasSdensity() & ...
                         this.HasSbulkModuls() & ...
                         this.HasSvolumeFraction(), ...
                         'There are parameters missing'); 
                  bool = 'True';
              otherwise
                  bool = False;
          end
    end
   
%    methods
%        function bool = HasSoundSpeed(this);
%        end
%         
% %         function outputArg = method1(obj,inputArg)
% %             %METHOD1 Summary of this method goes here
% %             %   Detailed explanation goes here
% %             outputArg = obj.Property1 + inputArg;
% %         end
%        
%    end
   end
end

